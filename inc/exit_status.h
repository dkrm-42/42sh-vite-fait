#ifndef EXIT_STATUS_H
# define EXIT_STATUS_H

int exit_status_get_last(void);
void exit_status_set_last(int exit_status);

#endif
